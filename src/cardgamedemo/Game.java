/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cardgamedemo;

/**
 *
 * @author SHUBHAM PADHYA
 */
public interface Game
{
        void playGame(int numberOfPlayers);

        void displayWinners();
}
