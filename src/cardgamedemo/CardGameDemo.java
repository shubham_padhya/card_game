/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package cardgamedemo;

/**
 *
 * @author SHUBHAM PADHYA
 */
import java.util.Scanner;

public class CardGameDemo {

    public CardGameDemo(){}
    public static void main(String[] args)
    {
            CardGame game = new CardGame();

            System.out.println("Card Game \n Player Options");
            System.out.println("1. Start Game \n 2.  Exit Game");
            System.out.print("Please provide your option : ");

            int num = 1;

            while (num != 0)
            {
                    Scanner sc = new Scanner(System.in);
                    num = sc.nextInt();

                    switch (num)
                    {
                            case 1:
                                    System.out.println("Provide the Number of Players( should be greater than 1 and less than 4) : ");
                                    sc = new Scanner(System.in);
                                    num = sc.nextInt();
                                    game.playGame(num);

                                    game.displayWinners();
                                    break;

                            case 2:
                                    System.exit(0);

                    }

                    System.out.println();
                    System.out.println("Card Game \n Select User Options");
                    System.out.println("1. Start Game \n2. Exit Game");
                    System.out.print("Please provide your option : ");
            }
    }
}